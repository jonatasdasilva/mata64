#!/usr/bin/python3
distancias_linha_reta = {
    1: {
        2: 10,
        3: 18.5,
        4: 24.8,
        5: 36.4,
        6: 38.8,
        7: 35.8,
        8: 25.4,
        9: 17.6,
        10: 9.1,
        11: 16.7,
        12: 27.3,
        13: 27.6,
        14: 29.8
    },
    2: {
        3: 8.5,
        4: 14.8,
        5: 26.6,
        6: 29.1,
        7: 26.1,
        8: 17.3,
        9: 10,
        10: 3.5,
        11: 15.5,
        12: 20.9,
        13: 19.1,
        14: 21.8
    },
    3: {
        4: 6.3,
        5: 18.2,
        6: 20.6,
        7: 17.6,
        8: 13.6,
        9: 9.4,
        10: 10.3,
        11: 19.5,
        12: 19.1,
        13: 12.1,
        14: 16.6
    },
    4: {
        5: 12,
        6: 14.4,
        7: 11.5,
        8: 12.4,
        9: 12.6,
        10: 16.7,
        11: 23.6,
        12: 18.6,
        13: 10.6,
        14: 15.4
    },
    5: {
        6: 3,
        7: 2.4,
        8: 19.4,
        9: 23.3,
        10: 28.2,
        11: 34.2,
        12: 24.8,
        13: 14.5,
        14: 17.9
    },
    6: {
        7: 3.3,
        8: 22.3,
        9: 25.7,
        10: 30.3,
        11: 36.7,
        12: 27.6,
        13: 15.2,
        14: 18.2
    },
    7: {
        8: 20,
        9: 23,
        10: 27.3,
        11: 34.2,
        12: 25.7,
        13: 12.4,
        14: 15.6
    },
    8: {
        9: 8.2,
        10: 20.3,
        11: 16.1,
        12: 6.4,
        13: 22.7,
        14: 27.6
    },
    9: {
        10: 13.5,
        11: 11.2,
        12: 10.9,
        13: 21.2,
        14: 26.6
    },
    10: {
        11: 17.6,
        12: 24.2,
        13: 18.7,
        14: 21.2
    },
    11: {
        12: 14.2,
        13: 31.5,
        14: 35.5
    },
    12: {
        13: 28.8,
        14: 33.6
    },
    13: {
        14: 5.1
    },
}

distancias_reais = {
    1: {
        2: 10
    },
    2: {
        3: 8.5,
        9: 10,
        10: 3.5
    },
    3: {
        4: 6.3,
        9: 9.4,
        13: 18.7
    },
    4: {
        5: 13,
        8: 15.3,
        13: 12.8
    },
    5: {
        6: 3,
        7: 2.4,
        8: 30
    },
    6: None,
    7: None,
    8: {
        9: 9.6,
        12: 6.4
    },
    9: {
        11: 12.2,
    },
    10: None,
    11: None,
    12: None,
    13: {
        14: 5.1
    },
    14: None
}

mapa = {
    1: [2],
    2: [1, 3, 9, 10],
    3: [2, 4, 9, 13],
    4: [3, 5, 8, 13],
    5: [4, 6, 8, 7],
    6: [5],
    7: [5],
    8: [4, 5, 9, 12],
    9: [2, 3, 8, 11],
    10: [2],
    11: [9],
    12: [8],
    13: [3, 4, 9, 14],
    14: [13]
}

linhas_trem = {
    "amarela": [10, 2, 9, 8, 5, 7],
    "azul": [1, 2, 3, 4, 5, 6],
    "vermelha": [11, 9, 3, 13],
    "verde": [12, 8, 4, 13, 14]
}

pontos_baldeacao = [2, 3, 4, 5, 8, 9, 13]

# função g(n) para a heuristica
def g(pai, atual):
    global distancias_reais

    for x in distancias_reais[pai]:
        if x == atual:
            #stringins = "dist "+str(pai)+" node: "+str(x)+": "+str(distancias_reais[pai][atual])
            #print(stringins)
            return distancias_reais[pai][atual]

# função h(n) para a heuristica
def h(atual, objetivo):
    global distancias_linha_reta

    for x in distancias_linha_reta[atual]:
        if x == objetivo:
            return distancias_linha_reta[atual][objetivo]

    return None

# se for necessário fazer uma poda no caminho
def poda(atual):
    global distancias_reais

    menor = 9223372036854775807
    estacao = 0
    for i in distancias_reais[atual]:
        if i and distancias_reais[atual][i]:
            if distancias_reais[atual][i] < menor:
                menor = distancias_reais[atual][i]
                estacao = i

    return estacao

# teste objetivo
def objetivo(t, atual):
    if t == atual:
        return True
    
    return False

# Calcula tempo real para impressão final
def calculaTempo(dist, baldeacao, v, u):
    distancia_real = (dist * 60) / v
    baldeacoes = baldeacao * u
    return (baldeacoes * distancia_real)


# verificar necessidade de baldeação
def baldeacao(pai, atual):
    global linhas_trem

    baldear = 0
    linha = ""
    lt = []
    
    for l in linhas_trem:
        if atual in linhas_trem[l]:
            linha = l
    
    atual = pai[atual]["pai"]

    while pai[atual]["pai"]:
        for l in linhas_trem:
            if atual in linhas_trem[l]:
                lt.append(l)
        if linha not in lt:
            baldear += 1
            linha = lt[0]
        atual = pai[atual]["pai"]

    return baldear


# Calcula o rota do objetivo até o pai
def rota(pai, e):
    rota = []
    rota.insert(0, e)

    while pai[e]["pai"]:
        rota.insert(0, pai[e]["pai"])
        e = pai[e]["pai"]
    
    return rota

def leitura():
    entradas = {"s": 0, "t": 0, "v": 0, "u": 0}
    entradas["s"], entradas["t"] = [int(y) for y in input().split()]
    entradas["v"] = int(input())
    entradas["u"] = int(input())

    return entradas

def main(entradas):
    global mapa

    fila = []
    caminho = [] # vetor com o caminho até a solução
    pai = {}
    saida = {}
    saidaAtual = {}

    fila.append((0, entradas["s"]))
    node = {
        "pai": None,
        "custo": 0,
        "dist": 0,
    }
    pai[entradas["s"]] = node
    #print(pai)

    if objetivo(entradas["t"], entradas["s"]):
            saidaAtual["caminho"] = caminho
            saidaAtual["rota"] = caminho
            saidaAtual["distancia"] = node["dist"]
            saidaAtual["baldeacao"] = 0
            saidaAtual["tempo"] = 0
            saida[entradas["s"]] = saidaAtual
            return saida
    print("while")
    while fila:
        fila.sort()
        v = fila.pop(0)
        print(v[1])
        if v not in caminho:
            caminho.append(v[1])
        for e in mapa[v[1]]:
            cust = g(v[1], e)
            fn = cust + h(e, entradas["t"])
            fila.append((fn, e))
            node["pai"] = v[1]
            node["custo"] = fn
            node["dist"] = cust + pai[v[1]]["dist"]
            if e in pai:
                if pai[e]["dist"] > node["dist"]:
                    pai[e] = node
            else:
                pai[e] = node
            if objetivo(entradas["t"], e):
                caminho.append(e)
                b = baldeacao(pai, e)
                saidaAtual["caminho"] = caminho
                saidaAtual["rota"] = rota(pai, v[1])
                saidaAtual["distancia"] = node["dist"]
                saidaAtual["baldeacao"] = b
                saidaAtual["tempo"] = calculaTempo(node["dist"], b, entradas["v"], entradas["u"])
                saida[e] = saidaAtual
                return saida
    
    return saida

if __name__ == "__main__":
    entradas = leitura()
    retorno = main(entradas)
    #imprimeResultado(solucao)
    print(retorno)
    ''' o retorno da impressão é o custo de minutos total, isso baseado na
    distância real. '''