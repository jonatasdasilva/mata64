#!/usr/bin/python3
# variáveis globais
#problema = {1: [2, 3, 4], 2: [1, 5, 6], 3: [1, 7], 4: [1, 8, 9, 10], 5: [2, 11, 12], 6: [2], 7: [3, 13, 14, 15], 8: [4, 16], 9: [4, 17, 18], 10: [4, 19, 20], 11: [5], 12: [5], 13: [7], 14: [7], 15: [7], 16: [8], 17: [9], 18: [9], 19: [10], 20: [10]}
problema = {}
X = 0
Y = 0

'''
enquanto a fila não estiver vazia
    retire um vértice v da fila
    para cada vizinho w de v
        se w não está numerado
            então numere w
                ponha w na fila
'''

def leitura():
    global problema, X, Y

    N, E = input().split()
    N = int(N)
    E = int(E)
    for i in range(N):
        problema[i + 1] = []
    for i in range(E):
        a, b = input().split()
        a = int(a)
        b = int(b)
        problema[a].append(b)
    X, Y = input().split()
    X = int(X)
    Y = int(Y)

def main(partida, objetivo):
    global problema
    fila = []
    caminho = [] # vetor com o caminho até a solução\
    fila.append(partida)
    caminho.append(partida)
    mark = [0] * len(problema)
    while fila:
        v = fila.pop(0)
        mark[v - 1] = 1
        if v not in caminho:
            caminho.append(v)
        if v == objetivo:
            return caminho
        for w in problema[v]:
            fila.append(w)
            if w not in caminho:
                caminho.append(w)
            if mark[w - 1] < 1:
                mark[w - 1] = 1
            if w == objetivo:
                return caminho
        mark[v - 1] = 2

def imprimeCaminho(caminho):
    saida = ""
    for index, item in enumerate(caminho, start=1):
        saida = saida + str(item)
        if index != len(caminho):
            saida = saida + "-"
    print(saida)

if __name__ == "__main__":
    leitura()
    caminho = main(X, Y)
    imprimeCaminho(caminho)