#!/usr/bin/python3
# variaveis globais
problema = []
n = 0

def leitura():
    global problema, n

    n = int(input())

    for x in range(n):
        line = [int(y) for y in input().split()]  
        problema.append(line)

def objetivo():
    global problema
    l1 = 0
    l0 = 0
    c1 = 0
    c0 = 0
    dPrincipal0 = 0
    dPrincipal1 = 0
    dSecundaria0 = 0
    dSecundaria1 = 0
    saida = []

    # verifica linha e diagonal principal
    for index, x in enumerate(problema):
        l1, l0 = 0, 0
        for indey, y in enumerate(x):
            if y == 1:
                l1 += 1
            if y == 0:
                l0 += 1
            if index == indey:
                if y == 1:
                    dPrincipal1 += 1
                elif y == 0:
                    dPrincipal0 += 1
            if indey == (n - index):
                if y == 1:
                    dSecundaria1 += 1
                elif y == 0:
                    dSecundaria0 += 1
            if problema[index][indey] == problema[index - 1][indey] and index > 0 and index < n:
                if y == 0:
                    c0 += 1
                if y == 1:
                    c1 += 1
            if l1 == n or l0 == n or c0 == n or c1 == n or dPrincipal0 == n or dPrincipal1 == n or dSecundaria0 == n or dSecundaria1 == n:
                saida.append(index)
                saida.append(indey)
                return saida
    ## falta verificar as colunas
        
def ninimax(game, state):
    player = game.move(state)
    value, move = max-value(game, state)
    return move
    #print("ninimax")

# se a entrada já tiver vencedor a saída é -1 -1
# se a 
def main():
    resultado = 1
    print("Main")

    return resultado

def imprimeResultado(solucao):
    saida = solucao[0]+","+solucao[1]
    print(saida)

if __name__ == "__main__":
    leitura()
    solucao = main()
    imprimeResultado(solucao)